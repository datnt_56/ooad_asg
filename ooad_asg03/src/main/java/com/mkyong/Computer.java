package com.mkyong;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;

/*
 * @author Giang
 */
public class Computer {

    private Shape handShape;

    public Shape get_shape() {
        return this.handShape;
    }

    public void set_shape(Shape shape) {
        handShape = shape;
    }

    public int selectNovice() {
        int choice;
        choice = (int) (Math.random() * 3 + 1);
        return choice;
    }

    public int selectVeteran(String key) {
        int countLine = 0;
        StringBuffer string;
        string = new StringBuffer();
        String line = null;
        try {
            FileReader fileReader = new FileReader("history.txt");
            BufferedReader bufferedReader = new BufferedReader(fileReader);

            while ((line = bufferedReader.readLine()) != null) {
                if (key == null || key.length() != 2) {
                    break;
                }

                if (key.charAt(0) == line.charAt(0) && key.charAt(1) == line.charAt(1)) {
                    string.append(line.charAt(2));
                    countLine++;
                }

            }
            bufferedReader.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        int choice;

        int value = countChar(string.toString());
        if (value == 2) {
            choice = 3;
        } else if (value == 1) {
            choice = 2;
        } else {
            choice = 1;
        }

        return choice;
    }

    public int countChar(String value) {
        int[] c = new int[3];
        int index;
        for (int i = 0; i < value.length(); i++) {
            if (value.charAt(i) == '1') {
                c[0]++;
            } else if (value.charAt(i) == '2') {
                c[1]++;
            } else {
                c[2]++;
            }
        }
        index = 0;
        for (int i = 1; i < 3; i++) {
            if (c[index] < c[i]) {
                index = i;
            }
        }
        return index;
    }
}
