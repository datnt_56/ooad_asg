package com.mkyong;

public class Match {

    // so lan thang cua nguoi choi
    private int win;

    // so lan hoa cua nguoi choi
    private int pie;

    // so lan thua
    private int fail;

    //so lan di trong 1 van
    private int timeInMatch;

    public Match(int t) {
        win = 0;
        pie = 0;
        fail = 0;
        timeInMatch = t;
    }

    public void setTimeInMatch(int t) {
        timeInMatch = t;
    }

    public int getTimeInMatch() {
        return timeInMatch;
    }

    // so sanh 2 hinh tay cua nguoi choi va may tinh
    public int equalShape(Shape u, Shape c) {

        int shapeU = u.getValue();
        int shapeC = c.getValue();

        if (shapeU + 1 == shapeC) {
            return -1;
        } else if (shapeU == shapeC) {
            return 0;
        } else {
            return 1;
        }

    }

    public void increasingWin() {
        win++;
    }

    public void increasingPie() {
        pie++;
    }

    public void increasingFail() {
        fail++;
    }

    public void setWin(int w) {
        win = w;
    }

    public void setPie(int p) {
        pie = p;
    }

    public void setFail(int f) {
        fail = f;
    }

    public int getWin() {
        return win;
    }

    public int getFail() {
        return fail;
    }

    public int getPie() {
        return pie;
    }
}
