/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong;

import java.util.Scanner;

/**
 *
 * @author dat
 */
public class RPSCommand {

    private Match match;
    private User user;
    private Computer computer;
    private Saver saver;
    

    public void init() {
        match = new Match(0);
        user = new User();
        computer = new Computer();
        saver = new Saver();
        

        Scanner scanner = new Scanner(System.in);
        System.out.println("Nhap so lan di trong 1 van ");

        int times = scanner.nextInt();
        help();

        user.select();
        int choice = user.get_choice();
        switch (choice) {
            case 0: {
                start(times);
                break;
            }
            case 4: {
                help();
                break;
            }
            case 6: {
                quit();
                break;
            }

        }
    }

    public void start(int times) {
        int counter = 0;
        int check;
        while (counter < times) {

            user.select();
            int choice = user.get_choice();
            Shape computerShape = new Shape(computer.selectNovice());
            String result = "";
            switch (choice) {
                case 1: {
                    user.set_shape(new Shape(1));
                    result += "1";
                    check = match.equalShape(user.get_shape(), computerShape);
                    if (check == 1) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thang");
                        match.increasingWin();
                    } else if (check == 0) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da hoa");
                        match.increasingPie();
                    } else {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thua");
                        match.increasingFail();
                    }
                    counter++;
                    break;
                }
                case 2: {
                    user.set_shape(new Shape(2));
                    result += "2";
                    check = match.equalShape(user.get_shape(), computerShape);
                    if (check == 1) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thang");
                        match.increasingWin();
                    } else if (check == 0) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da hoa");
                        match.increasingPie();
                    } else {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thua");
                        match.increasingFail();
                    }
                    counter++;
                    break;
                }
                case 3: {
                    user.set_shape(new Shape(3));
                    result += "3";
                    check = match.equalShape(user.get_shape(), computerShape);
                    if (check == 1) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thang");
                        match.increasingWin();
                    } else if (check == 0) {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da hoa");
                        match.increasingPie();
                    } else {
                        System.out.println("May tinh ra" + computerShape.getShape());
                        System.out.println("Ban da thua");
                        match.increasingFail();
                    }
                    counter++;
                    break;
                }
                case 4: {
                    help();
                    break;
                }
                case 5: {
                    askScore();
                    break;
                }
                case 6: {
                    quit();
                    break;
                }
                case 7: {
                    restart();
                    break;
                }
            }
            if (result.length() == 3) {
                saveUserChoice(result);
                result = "";
            }
        }
    }

    public void restart() {
        match.setWin(0);
        match.setFail(0);
        match.setPie(0);
    }

    public void quit() {
        System.exit(0);
    }

    public void help() {
        System.out.println("----------------------------------------------------");
        System.out.println("*                    Huong dan choi game           *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 0 de bat dau choi                *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 1 de chon dam                    *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 2 de chon la                     *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 3 de chon keo                    *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 4 de xem tro giup                *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 6 de thoat khoi game             *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 5 de xem diem                    *");
        System.out.println("----------------------------------------------------");
        System.out.println("*            Nhap 7 de khoi dong lai game          *");
        System.out.println("----------------------------------------------------");
    }

    public void askScore() {
        System.out.println("----------------------------------------------------");
        System.out.println("|              |                 |                 |");
        System.out.println("|    Thang     |       Hoa       |        Thua     |");
        System.out.println("----------------------------------------------------");
        System.out.println("|      "+ match.getWin()+ "       |         "+ match.getPie()+ "       |         "+ match.getFail()+ "       |");
        System.out.println("|              |                 |                 |");              
        System.out.println("----------------------------------------------------");

    }

    public void saveUserChoice(String resultUser) {
        saver.add(resultUser);
    }

}
