package com.mkyong;

import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import javax.swing.JLabel;

import javax.swing.JOptionPane;

/**
 *
 * @author GiGi
 */
public class RPSGraphics extends javax.swing.JFrame {

    User user = new User();
    Saver save = new Saver();
    Computer computer = new Computer();
    StringBuffer temp = new StringBuffer();
    Match match = new Match(0);
    private int count = 0;
    boolean onGame = false;

    public RPSGraphics() {
        initComponents();
    }

    @SuppressWarnings("unchecked")

    private void initComponents() {

        bRock = new javax.swing.JButton();
        bPaper = new javax.swing.JButton();
        bScissors = new javax.swing.JButton();
        jLabel2 = new javax.swing.JLabel();
        jlComputer = new javax.swing.JLabel();
        jlUser = new javax.swing.JLabel();
        labelComputer = new javax.swing.JLabel();
        labelUser = new javax.swing.JLabel();
        jPanel1 = new javax.swing.JPanel();
        jCheckBox1 = new javax.swing.JCheckBox();
        jCheckBox2 = new javax.swing.JCheckBox();
        jLabel7 = new javax.swing.JLabel();
        tTimes = new javax.swing.JTextField();
        bStart = new javax.swing.JButton();
        jLabel1 = new javax.swing.JLabel();
        jLabel3 = new javax.swing.JLabel();
        jLabel4 = new javax.swing.JLabel();
        jLabel5 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        tfWin = new javax.swing.JTextField();
        tfPie = new javax.swing.JTextField();
        tfFail = new javax.swing.JTextField();
        tfCur = new javax.swing.JTextField();
        jlRound = new javax.swing.JLabel();
        jMenuBar1 = new javax.swing.JMenuBar();
        mRestart = new javax.swing.JMenu();
        mHelp = new javax.swing.JMenu();
        mQuit = new javax.swing.JMenu();

        setDefaultCloseOperation(javax.swing.WindowConstants.EXIT_ON_CLOSE);
        bRock.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bRock.setText("Rock");

        bPaper.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bPaper.setText("Paper");

        bScissors.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        bScissors.setText("Scissors");
        enableButtons();
        jLabel2.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jLabel2.setText("\n\n  Times");

        jlComputer.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jlComputer.setText("\n\n        Computer");

        jlUser.setFont(new java.awt.Font("Tahoma", 1, 15)); // NOI18N
        jlUser.setText("\n\n    You");

        jlRound.setFont(new java.awt.Font("Tahoma", 1, 15));
        jPanel1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        jCheckBox1.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jCheckBox1.setText("Novice");

        jCheckBox2.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jCheckBox2.setText("Veteran");

        jLabel7.setFont(new java.awt.Font("Tahoma", 0, 12)); // NOI18N
        jLabel7.setText("Times");

        bStart.setText("Start");
        bStart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                bStartActionPerformed(evt);
            }
        });

        jLabel1.setText("You' s Score:");

        jLabel3.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel3.setText("Round");

        jLabel4.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel4.setText("Fails");

        jLabel5.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel5.setText("Pies");

        jLabel6.setFont(new java.awt.Font("Tahoma", 1, 12)); // NOI18N
        jLabel6.setText("Wins");

        tfWin.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                tfWinActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout jPanel1Layout = new javax.swing.GroupLayout(jPanel1);
        jPanel1.setLayout(jPanel1Layout);
        jPanel1Layout.setHorizontalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addGap(17, 17, 17)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                                                        .addGap(18, 18, 18)
                                                        .addComponent(tTimes))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jCheckBox2)
                                                                .addComponent(jCheckBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 95, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                        .addGap(0, 25, Short.MAX_VALUE))))
                                .addGroup(jPanel1Layout.createSequentialGroup()
                                        .addContainerGap()
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel5)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(tfPie, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                                .addComponent(jLabel1)
                                                                .addComponent(jLabel6))
                                                        .addGap(0, 0, Short.MAX_VALUE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel4)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(tfFail, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))
                                                .addGroup(jPanel1Layout.createSequentialGroup()
                                                        .addComponent(jLabel3)
                                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                                        .addComponent(tfCur, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))
                                .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, jPanel1Layout.createSequentialGroup()
                                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                                .addComponent(bStart, javax.swing.GroupLayout.Alignment.TRAILING)
                                                .addComponent(tfWin, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE))))
                        .addContainerGap())
        );

        jPanel1Layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{jCheckBox1, jCheckBox2});

        jPanel1Layout.setVerticalGroup(
                jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(jPanel1Layout.createSequentialGroup()
                        .addGap(11, 11, 11)
                        .addComponent(jCheckBox1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                        .addComponent(jCheckBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 23, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(tTimes, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGap(18, 18, 18)
                        .addComponent(bStart, javax.swing.GroupLayout.PREFERRED_SIZE, 30, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 26, Short.MAX_VALUE)
                        .addComponent(jLabel1)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel6)
                                .addComponent(tfWin, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel5)
                                .addComponent(tfPie, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel4)
                                .addComponent(tfFail, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(jPanel1Layout.createParallelGroup(javax.swing.GroupLayout.Alignment.CENTER)
                                .addComponent(jLabel3)
                                .addComponent(tfCur, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addContainerGap())
        );

        jMenuBar1.setBorder(javax.swing.BorderFactory.createLineBorder(new java.awt.Color(0, 0, 0)));

        mRestart.setText("Restart");
        mRestart.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mRestartMouseClicked(evt);
            }
        });
        mRestart.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            }
        });
        jMenuBar1.add(mRestart);

        mHelp.setText("Help");
        mHelp.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mHelpMouseClicked(evt);
            }
        });
        mHelp.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {

            }
        });
        jMenuBar1.add(mHelp);

        mQuit.setText("Quit");
        mQuit.addMouseListener(new java.awt.event.MouseAdapter() {
            public void mouseClicked(java.awt.event.MouseEvent evt) {
                mQuitMouseClicked(evt);
            }
        });
        mQuit.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                mQuitActionPerformed(evt);
            }
        });
        jMenuBar1.add(mQuit);

        setJMenuBar(jMenuBar1);

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(33, 33, 33)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(bRock)
                                .addComponent(jlUser, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(41, 41, 41)
                                        .addComponent(bPaper, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                                        .addGap(9, 9, 9))
                                .addGroup(javax.swing.GroupLayout.Alignment.LEADING, layout.createSequentialGroup()
                                        .addGap(25, 25, 25)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 58, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGap(18, 18, 18)
                                        .addComponent(jlRound, javax.swing.GroupLayout.PREFERRED_SIZE, 22, javax.swing.GroupLayout.PREFERRED_SIZE)))
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addGroup(layout.createSequentialGroup()
                                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 38, Short.MAX_VALUE)
                                        .addComponent(bScissors))
                                .addGroup(layout.createSequentialGroup()
                                        .addGap(25, 25, 25)
                                        .addComponent(jlComputer, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)))
                        .addGap(40, 40, 40)
                        .addComponent(jPanel1, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGroup(layout.createSequentialGroup()
                        .addGap(29, 29, 29)
                        .addComponent(labelUser, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addComponent(labelComputer, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(0, 0, Short.MAX_VALUE))
        );

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{bPaper, bRock, bScissors});

        layout.linkSize(javax.swing.SwingConstants.HORIZONTAL, new java.awt.Component[]{labelComputer, labelUser});

        layout.setVerticalGroup(
                layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                .addGroup(layout.createSequentialGroup()
                        .addGap(25, 25, 25)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                .addComponent(bPaper)
                                .addComponent(bRock, javax.swing.GroupLayout.PREFERRED_SIZE, 34, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(bScissors))
                        .addGap(15, 15, 15)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                        .addComponent(jLabel2, javax.swing.GroupLayout.Alignment.TRAILING, javax.swing.GroupLayout.PREFERRED_SIZE, 31, javax.swing.GroupLayout.PREFERRED_SIZE)
                                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                                                .addComponent(jlComputer)
                                                .addComponent(jlUser)))
                                .addComponent(jlRound))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                                .addComponent(labelUser, javax.swing.GroupLayout.PREFERRED_SIZE, 165, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addComponent(labelComputer))
                        .addGap(0, 0, Short.MAX_VALUE))
                .addComponent(jPanel1, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
        );

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{bPaper, bRock, bScissors});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{jLabel2, jlComputer, jlRound, jlUser});

        layout.linkSize(javax.swing.SwingConstants.VERTICAL, new java.awt.Component[]{labelComputer, labelUser});

        pack();
    }

    private void bStartActionPerformed(java.awt.event.ActionEvent evt) {

        String times = tTimes.getText();

        int ntimes;
        try {
            ntimes = Integer.parseInt(times);

        } catch (NumberFormatException e) {
            JOptionPane.showMessageDialog(null, "Wrong format");
            tTimes.requestFocus();
            return;
        }
        boolean cb1 = jCheckBox1.isSelected();
        boolean cb2 = jCheckBox2.isSelected();
        if ((cb1 == true && cb2 == true) || (cb1 == false && cb2 == false)) {
            JOptionPane.showMessageDialog(null, "CheckBox error !!!");
            jCheckBox1.requestFocus();
            return;
        }

        onGame = true;
        enableButtons();
        match = new Match(ntimes);
        jlRound.setText("");
        tfWin.setText(null);
        tfCur.setText(null);
        tfFail.setText(null);
        tfPie.setText(null);
        tTimes.setText(null);
    }


    private void mQuitActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_mQuitActionPerformed
        System.exit(0);
    }//GEN-LAST:event_mQuitActionPerformed

    private void mHelpMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mHelpMouseClicked
        JOptionPane.showMessageDialog(null, "First, You choose from two different modes: \n\"Novice\", where the computer learns to play from scratch,  \n\"Veteran\", where the computer pits over many rounds of previous experience against you.\nThen, Enter \"times\" in a match and click \"Start\".", "Help", 0);

	}//GEN-LAST:event_mHelpMouseClicked

    private void mRestartMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mRestartMouseClicked
        onGame = false;
        enableButtons();
        tTimes.setText(null);
        jCheckBox1.setSelected(onGame);
        jCheckBox2.setSelected(onGame);
        match.setWin(0);
        match.setPie(0);
        match.setFail(0);
        jlRound.setText("");
        tfWin.setText(null);
        tfCur.setText(null);
        tfFail.setText(null);
        tfPie.setText(null);
        labelComputer.setIcon(null);
        labelUser.setIcon(null);
    }//GEN-LAST:event_mRestartMouseClicked

    private void mQuitMouseClicked(java.awt.event.MouseEvent evt) {//GEN-FIRST:event_mQuitMouseClicked
        System.exit(0);
    }//GEN-LAST:event_mQuitMouseClicked

    private void tfWinActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_tfWinActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_tfWinActionPerformed
    public void enableButtons() {
        bStart.setEnabled(!onGame);
        bPaper.setEnabled(onGame);
        bRock.setEnabled(onGame);
        bScissors.setEnabled(onGame);
    }

    public void play() {
        ActionListener evt = new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                boolean cb1 = jCheckBox1.isSelected();
                boolean cb2 = jCheckBox2.isSelected();
                ShapeGraphics shapeUser = null;
                int check;
                
                String key = null;
                if (e.getSource() == bRock) {
                    shapeUser = new ShapeGraphics(1);
                }
                if (e.getActionCommand() == "Paper") {
                    shapeUser = new ShapeGraphics(2);
                }
                if (e.getActionCommand() == "Scissors") {
                    shapeUser = new ShapeGraphics(3);
                }

                user.set_shape(shapeUser);
                shapeUser.setLabel(labelUser);
                shapeUser.getShape();
                count++;
                temp.append(shapeUser.getValue());
                if (count % 2 == 0) {
                    save.add(temp.toString());
                    temp = new StringBuffer();
                }

                int com=-1;
                if (cb1 == true) {
                    com = computer.selectNovice();
                } else if (cb2 == true) {
                    com = computer.selectVeteran(key);
                }
                ShapeGraphics shapeComputer = new ShapeGraphics(com);
                computer.set_shape(shapeComputer);
                int n = match.getTimeInMatch()-count;
                jlRound.setText(""+ n );
                shapeComputer.setLabel(labelComputer);
                shapeComputer.getShape();
                if (count % 2 != 0) {
                    temp.append(shapeComputer.getValue());
                }
                
                key = temp.toString();
                check = match.equalShape(shapeUser, shapeComputer);
                if (check == 1) {
                    match.increasingWin();
                    tfWin.setText(""+match.getWin());
                    tfCur.setText("Win");
         
                } else if (check == 0) {
                    match.increasingPie();
                    tfPie.setText(""+match.getPie());
                    tfCur.setText("Pie");
                } else {
                    match.increasingFail();
                    tfFail.setText(""+match.getFail());
                    tfCur.setText("Fail");
                }
                if (count == match.getTimeInMatch()) {
                    onGame = false;
                    enableButtons();
                    count=0;
                    
                }

            }

        };
        bPaper.addActionListener(evt);
        bRock.addActionListener(evt);
        bScissors.addActionListener(evt);
    }

    /**
     * @param args the command line arguments
     */

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton bPaper;
    private javax.swing.JButton bRock;
    private javax.swing.JButton bScissors;
    private javax.swing.JButton bStart;
    private javax.swing.JCheckBox jCheckBox1;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel4;
    private javax.swing.JLabel jLabel5;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JMenuBar jMenuBar1;
    private javax.swing.JPanel jPanel1;
    private javax.swing.JLabel jlComputer;
    private javax.swing.JLabel jlRound;
    private javax.swing.JLabel jlUser;
    private javax.swing.JLabel labelComputer;
    private javax.swing.JLabel labelUser;
    private javax.swing.JMenu mHelp;
    private javax.swing.JMenu mQuit;
    private javax.swing.JMenu mRestart;
    private javax.swing.JTextField tTimes;
    private javax.swing.JTextField tfCur;
    private javax.swing.JTextField tfFail;
    private javax.swing.JTextField tfPie;
    private javax.swing.JTextField tfWin;
    // End of variables declaration//GEN-END:variables
}
