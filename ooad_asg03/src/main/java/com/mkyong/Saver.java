package com.mkyong;
import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;


public class Saver{
        private final String fileName = "history.txt";
	public void add(String value){
		try{
                    FileWriter fileWrider = new FileWriter(fileName, true);
                    try (BufferedWriter bufferedWriter = new BufferedWriter(fileWrider)) {
                        bufferedWriter.write(value);
                        bufferedWriter.newLine();
                    }
                }
                catch(IOException e){
                    e.printStackTrace();
                }
                

	}
        public void printfFile(){
            String line = null;
            try{
                FileReader fileReader = new FileReader(fileName);
                try (BufferedReader bufferedReader = new BufferedReader(fileReader)) {
                    while((line = bufferedReader.readLine()) != null){
                        System.out.println(line);
                    }
                }
            }
            catch(IOException e){
                e.printStackTrace();
            }
        }
	
}