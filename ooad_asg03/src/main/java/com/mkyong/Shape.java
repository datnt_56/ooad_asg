/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.mkyong;

/**
 *
 * @author dat
 */
public class Shape {

    int value;
    public static final int Rock = 1;
    public static final int Paper = 2;
    public static final int Sissors = 3;

    public Shape() {
    }

    public Shape(int i) {
        switch (i) {
            case 1: {
                value = Rock;
                break;
            }
            case 2: {
                value = Paper;
                break;
            }
            case 3: {
                value = Sissors;
                break;
            }
        }
    }

    protected void setValue(int v) {
        value = v;
    }

    public int getValue() {
        return value;
    }

    public String getShape() {
        int n = getValue();
        if (n == 1) {
            return "Rock";
        } else if (n == 2) {
            return "Paper";
        } else {
            return "Sissors";
        }
    }

}
