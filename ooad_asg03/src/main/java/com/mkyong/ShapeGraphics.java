/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

package com.mkyong;

import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import javax.swing.ImageIcon;
import javax.swing.JLabel;

/**
 *
 * @author GiGi
 */
public class ShapeGraphics extends Shape {
    
    private JLabel label;
    public void setLabel (JLabel jl)
    {
        label = jl;
    }
    ShapeGraphics(int Rock) {
        super(Rock);
    }
    public void setImage(JLabel label, String filename)
    {
        try {
            BufferedImage image =ImageIO.read(new File(filename));
            int x = label.getSize().width;
            int y = label.getSize().height;
            int ix = image.getWidth();
            int iy = image.getHeight();
            int dx = 0;
            int dy = 0;
            if (x/y > ix / iy)
            {
                dy =y;
                dx = dy*ix /iy;
            }
            else
            {
                dx = x;
                dy = dx * iy / ix;
            }
            ImageIcon icon = new ImageIcon (image.getScaledInstance(dx, dy, image.SCALE_SMOOTH));
            label.setIcon(icon);
        } catch (IOException ex) {
            Logger.getLogger(ShapeGraphics.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    public String getShape(){
        int n = getValue();
        if ( n == 1){
            setImage(label, "dam.png");
            return " ";
        }
        else if(n == 2){
            setImage(label, "la.png");
            return " ";
        }
        else{
            setImage(label, "keo.png");
            return " ";
        }
    }
}
