package com.mkyong;

import java.util.Scanner;

public class User {
	private Shape handShape;
	private int choice;

	public void set_shape(Shape shape){
		handShape = shape;
	}
	public Shape get_shape() {
		return this.handShape;
	}
        
        public int get_choice(){
            return choice;
        }
	public void select() {
		Scanner input;
		input = new Scanner(System.in);
		do {
			System.out.println("Moi nhap lua chon cua ban: ");
			choice = input.nextInt();

			if (choice >= 0 && choice <= 7)
				break;
			else
				System.out.print("Ban da nhap sai moi nhap lai. !!!\n");
		} while (choice < 0 || choice > 7);

	}
}
